const CleanWebpackPlugin = require('clean-webpack-plugin');
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const path = require('path');
const process = require('process');
const webpack = require('webpack');

const isProduction = process.argv.indexOf('-p') >= 0;
const package = require('./package.json');
const localhostOnly = false;

/** @type {StaticServerProperties} */
const serverProps = {
	version: `v${package.version} pre-alpha${isProduction ? "" : " (development)"}`
};

/** @type {StaticClientProperties} */
const clientProps = {
	peerJSAPIKey: '0iqsj57i8hestt9'
};

module.exports = {
	entry: {
		// TODO: delete 'dist/~static.SOMECHUNKHASH.js' after build
		'~static': './src/build/static.tsx',  // temporary; only used to generate the static website
		'script': './src/script.ts'
	},
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: '[name].[chunkhash].js',
		libraryTarget: 'umd'
	},
	resolve: {
		extensions: ['.js', '.ts', '.tsx']
	},
	module: {
		rules: [{
			test: /\.(ts|tsx)$/,
			use: isProduction ? ['uglify-loader', 'ts-loader'] : ['ts-loader']
		}, {
			test: /\.less$/,
			use: isProduction ? ['extract-loader', 'css-loader?minimize=true', 'less-loader'] : ['less-loader']
		}, {
			test: /\.png$/,
			use: ['file-loader?name=[name].[hash].[ext]']
		}]
	},
	target: 'node',
	plugins: [
		new StaticSiteGeneratorPlugin({
			entry: '~static',
			paths: ['/'],
			crawl: true,
			locals: { serverProps, clientProps }
		}),
		...isProduction ? [
			new CleanWebpackPlugin(['dist'], {
				root: __dirname
			}),
			new webpack.optimize.UglifyJsPlugin({
				compress: { warnings: false }
			})
		 ] : []
	],
	devServer: {
		host: localhostOnly ? 'localhost' : '0.0.0.0',
		disableHostCheck: localhostOnly ? false : true,
		port: 8080,
		inline: false
	}
};
