/// <reference path="../types.d.ts" />

import ActionsBox from "./interface/actions_box";
import ModalDialog from "./interface/modal_dialog";
import * as util from "./interface/util";
import Connection from "./network/connection";
import Table from "./specific/table";

const peerID = document.getElementById('peerID') as HTMLElement;
const connectionStatus = document.getElementById('connectionStatus') as HTMLElement;
const connectButton = document.getElementById('connectButton') as HTMLButtonElement;
const connnectIDDialog = document.getElementById('connnectIDDialog') as HTMLElement;
const connnectIDDialogIDInput = document.getElementById('connnectIDDialogIDInput') as HTMLInputElement;
const connnectIDDialogCancelButton = document.getElementById('connnectIDDialogCancelButton') as HTMLElement;
const connnectIDDialogConfirmButton = document.getElementById('connnectIDDialogConfirmButton') as HTMLButtonElement;
const connectUnsavedDialog = document.getElementById('connectUnsavedDialog') as HTMLElement;
const connectUnsavedDialogCancelButton = document.getElementById('connectUnsavedDialogCancelButton') as HTMLElement;
const connectUnsavedDialogConfirmButton = document.getElementById('connectUnsavedDialogConfirmButton') as HTMLButtonElement;
const createButton = document.getElementById('createButton') as HTMLButtonElement;
const disconnectButton = document.getElementById('disconnectButton') as HTMLButtonElement;
const peersButton = document.getElementById('peersButton') as HTMLButtonElement;
const peersDialog = document.getElementById('peersDialog') as HTMLElement;
const peersDialogList = document.getElementById('peersDialogList') as HTMLElement;
const saveButton = document.getElementById('saveButton') as HTMLElement;
const loadButton = document.getElementById('loadButton') as HTMLElement;
const loadDialog = document.getElementById('loadDialog') as HTMLElement;
const loadDialogCancelButton = document.getElementById('loadDialogCancelButton') as HTMLButtonElement;
const loadDialogConfirmButton = document.getElementById('loadDialogConfirmButton') as HTMLButtonElement;
const clearButton = document.getElementById('clearButton') as HTMLButtonElement;
const clearDialog = document.getElementById('clearDialog') as HTMLElement;
const clearDialogCancelButton = document.getElementById('clearDialogCancelButton') as HTMLButtonElement;
const clearDialogConfirmButton = document.getElementById('clearDialogConfirmButton') as HTMLButtonElement;
const imagesButton = document.getElementById('imagesButton') as HTMLElement;
const imagesManageDialog = document.getElementById('imagesManageDialog') as HTMLElement;
const aboutButton = document.getElementById('aboutButton') as HTMLButtonElement;
const aboutDialog = document.getElementById('aboutDialog') as HTMLElement;
const tableSpace = document.getElementById('tableSpace') as HTMLElement;

class ImagesCollection {
}

const modalDialog = new ModalDialog();
const actionsBox = new ActionsBox();
const connection = new Connection();
const table = new Table(tableSpace);

connection.onMessage = (sender, message) => {
	
};

connection.onAuthenticating = () => {
	console.log("Authenticating");
	connectionStatus.innerText = `Creating a new empty room…`;
	peerID.innerText = `working…`;
};
connection.onAuthenticated = id => {
	console.log("Authenticated", id);
	connectionStatus.innerText = `New empty room created`;
	peerID.innerText = id;
	disconnectButton.disabled = false;
	peersButton.disabled = false;
};
connection.onDisconnected = id => {
	console.log("Disconnected", id);
	connectionStatus.innerText = `Disconnected`;
	peerID.innerText = `offline`;
	disconnectButton.disabled = true;
	peersButton.disabled = true;
};
connection.onJoined = id => {
	console.log("Joined", id);
	connectionStatus.innerText = `"${id}" joined this room`;
};
connection.onError = (error, fatal) => {
	console.error("Error", fatal ? "(fatal)" : "(non-fatal)", error);
	connectionStatus.innerText = `${fatal ? "FATAL " : ""}${error}`;
};




connectButton.addEventListener('click', e => {
	modalDialog.open("Join Room", connnectIDDialog, true);
});
connnectIDDialogCancelButton.addEventListener('click', e => {
	modalDialog.close();
});
connnectIDDialogConfirmButton.addEventListener('click', e => {
	modalDialog.close();

	if (!table.isEmpty()) {
		modalDialog.open("Unsaved Data", connectUnsavedDialog, true);
		return;
	}
		
	connection.join(connnectIDDialogIDInput.value);
});
connectUnsavedDialogCancelButton.addEventListener('click', e => {
	modalDialog.close();
});
connectUnsavedDialogConfirmButton.addEventListener('click', e => {
	modalDialog.close();
	connection.join(connnectIDDialogIDInput.value);
});

createButton.addEventListener('click', e => {
	connection.createNew();
});

disconnectButton.addEventListener('click', e => {
	connection.disconnect();
});

peersButton.addEventListener('click', e => {
	while (peersDialogList.hasChildNodes())
		peersDialogList.removeChild(peersDialogList.lastChild as Node);

	const peers = [`${connection.getID()} (you)`].concat(connection.getPeers());
	for (const peer of peers) {
		const li = document.createElement('li');
		li.innerText = peer;
		peersDialogList.appendChild(li);
	}
	
	modalDialog.open(`${peers.length} ${peers.length == 1 ? "Person" : "People"}`, peersDialog, true);
});

saveButton.addEventListener('click', e => {
	util.downloadBlob(table.getBlob(), `sharedboard_${connection.getID() || "offline"}_${new Date().getTime()}${Math.round(Math.random()*89999+10000)}.dat`);
});

let loadTable: Blob;
loadButton.addEventListener('click', e => {
	util.pickFile(file => {
		loadTable = file;

		if (!table.isEmpty()) {
			modalDialog.open("Unsaved Data", loadDialog, true);
			return;
		}

		table.loadBlob(loadTable);
	});
});
loadDialogCancelButton.addEventListener('click', e => {
	modalDialog.close();
});
loadDialogConfirmButton.addEventListener('click', e => {
	modalDialog.close();
	table.loadBlob(loadTable);
});

clearButton.addEventListener('click', e => {
	if (!table.isEmpty() && connection.getPeers().length) {
		modalDialog.open("Unsaved Data", clearDialog, true);
		return;
	}

	table.clear();
});
clearDialogCancelButton.addEventListener('click', e => {
	modalDialog.close();
});
clearDialogConfirmButton.addEventListener('click', e => {
	modalDialog.close();
	table.clear();
});

imagesButton.addEventListener('click', e => {
	modalDialog.open("Images Collection", imagesManageDialog, true);
});

aboutButton.addEventListener('click', e => {
	modalDialog.open("About", aboutDialog, true);
});

tableSpace.addEventListener('contextmenu', e => {
	e.preventDefault();

	actionsBox.open(e.clientX, e.clientY, [
		{ name: "Test", action: () => alert("test") },
		{ name: "Hello, World", action: () => alert("hello, world!") },
	]);
});

window.addEventListener('resize', () => table.recompute());
table.recompute();
