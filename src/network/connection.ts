/// <reference path="../../types.d.ts" />

import Message, { JSONValue, MessageType, ServiceMessageAction } from "./message";

const INTERVAL_BETWEEN_PINGS = 8000;  // 8 seconds
const CONNECTION_TIMEOUT = 60000;  // 60 seconds

interface PeerStatus {
	connection: PeerJs.DataConnection,
	lastMessageReceived: number;
	lastPing: number;
}

export default class Connection {

	/** List of connected peers, not including self. */
	private peers: PeerStatus[] = [];
	/** This connection's peer, if any. */
	private peer?: PeerJs.Peer;
	/** Interval used to keep connections alive. */
	private intervalID?: any;
	
	/** Fired when trying to get an ID. */
	public onAuthenticating: () => void;
	/** Fired when got an ID. */
	public onAuthenticated: (newID: string) => void;
	/** Fired when disconnected from other peers, if any, and dropped our ID. */
	public onDisconnected: (previousID: string) => void;
	/** Fired when another peer is connecting here. */
	public onJoined: (peerID: string) => void;
	/** Fired when another peer is disconnecting from here. */
	public onLeft: (peerID: string) => void;
	/** Fired when something bad happened. */
	public onError: (error: string, /* `true` when `error` closed the connection. */fatal: boolean) => void;
	/** Fired when a message is received from a connected peer. */
	public onMessage: (senderID: string, data: JSONValue) => void;

	/** ID of this connection's peer, if any. */
	public getID(): string|undefined {
		return this.peer ? this.peer.id : undefined;
	}

	/** IDs of connected peers, not including self. */
	public getPeers(): string[] {
		return this.peers.map(peer => peer.connection.peer);
	}

	/** Is this connection open and listening for incoming messages? */
	public isConnected(): boolean {
		return !!this.peer;
	}

	/** Close current connection, if any, and create a new one. */
	public createNew(): void {
		this.disconnect();
		
		this.peer = new Peer({ key: props.peerJSAPIKey });
		this.peer.on('error', error => {
			this.disconnect();
			this.onError(`${error}`, true);
		});
		this.peer.on('open', id => this.onAuthenticated(id));
		this.peer.on('connection', connection => {
			this.registerPeer(connection);
		});

		this.intervalID = setInterval(() => {
			const now = new Date().getTime();
			for (const peer of this.peers) {
				if (now > Math.max(peer.lastPing, peer.lastMessageReceived) + INTERVAL_BETWEEN_PINGS) {
					peer.lastPing = now;
					this.sendOne(peer, { type: MessageType.Service, action: ServiceMessageAction.Ping });
				}

				if (now > peer.lastMessageReceived + CONNECTION_TIMEOUT)
					this.unregisterPeer(peer);
			}
		}, INTERVAL_BETWEEN_PINGS * .5);

		this.onAuthenticating();
	}

	/** Close current connection, if any, and create a new one with another peer. */
	public join(id: string): void {
		this.createNew();
		const peer = this.peer as PeerJs.Peer;
		peer.on('open', () => {
			const connection = peer.connect(id, { reliable: true });
			this.registerPeer(connection);
		});
	}

	/** Close current connection. */
	public disconnect(): void {
		if (!this.peer)
			return;

		const id = this.peer.id;

		this.sendAll({ type: MessageType.Service, action: ServiceMessageAction.Disconnect });
		this.peer.destroy();
		this.peer = undefined;

		this.peers = [];

		if (this.intervalID) {
			clearInterval(this.intervalID);
			this.intervalID = 0;
		}

		this.onDisconnected(id);
	}

	/** Send a message to all connected peers. */
	public send(data: JSONValue) {
		this.sendAll({ type: MessageType.Ordinary, data });
	}

	/** Send a low-level message to all connected peers. */
	private sendAll(message: Message) {
		for (const peer of this.peers)
			this.sendOne(peer, message);
	}

	/** Send a low-level message to just a single peer. */
	private sendOne(peer: PeerStatus, message: Message) {
		peer.connection.send(message);
	}

	/** Listen for messages from a `connection` with another peer and keep connection alive. */
	private registerPeer(connection: PeerJs.DataConnection) {
		connection.on('error', error => this.onError(`${error}`, false));
		connection.on('open', () => {
			if (!this.peers.every(peer => peer.connection.peer != connection.peer))
				return;  // Already connected

			const now = new Date().getTime();
			const peer = { connection, lastMessageReceived: now, lastPing: now };
			this.peers.push(peer);

			this.onJoined(connection.peer);

			connection.on('close', () => this.unregisterPeer(peer));
			connection.on('data', (message: Message) => {
				if (this.peers.indexOf(peer) < 0)
					return;  // not connected

				const now = new Date().getTime();
				peer.lastMessageReceived = now;

				switch (message.type) {
				case MessageType.Service:
					switch (message.action) {
					case ServiceMessageAction.Disconnect:
						return this.unregisterPeer(peer);
					case ServiceMessageAction.Ping:
						return this.sendOne(peer, { type: MessageType.Service, action: ServiceMessageAction.Pong });
					case ServiceMessageAction.Pong:
						return;  // nothing to do, this is just a keep-alive
					default:
						return this.onError(`unexpected service message action from ${JSON.stringify(peer.connection.peer)}: ${JSON.stringify(message.action as any)}`, false);
					}
				case MessageType.Ordinary:
					return this.onMessage(connection.peer, message.data);
				default:
					return this.onError(`unexpected message type from ${JSON.stringify(peer.connection.peer)}: ${JSON.stringify((message as any).type)}`, false);
				}
			});
		});
	}

	/** Stop listening for messages from `peer` and send a disconnection message. */
	private unregisterPeer(peer: PeerStatus) {
		const i = this.peers.indexOf(peer);
		if (i < 0)
			return;  // Already disconnected
		
		this.peers.splice(i, 1);

		peer.connection.send({ type: MessageType.Service, action: ServiceMessageAction.Disconnect });
		peer.connection.close();

		this.onLeft(peer.connection.peer);
	}
}
