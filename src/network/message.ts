/// <reference path="../../types.d.ts" />

export type JSONValue = string | number | boolean | null | { [key: string]: JSONValue };

export enum MessageType {
	Service = 'svc',
	Ordinary = 'msg'
}

export enum ServiceMessageAction {
	/** «I am going offline, remove me from your peers list» */
	Disconnect = 'disconnect',
	/** «Are you still there?» */
	Ping = 'ping',
	/** «I am still here» */
	Pong = 'pong'
}

export interface ServiceMessage {
	type: MessageType.Service,
	action: ServiceMessageAction
}

export interface OrdinaryMessage {
	type: MessageType.Ordinary,
	data: JSONValue
}

type Message = ServiceMessage | OrdinaryMessage;
export default Message;
