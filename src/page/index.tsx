/// <reference path="../../types.d.ts" />

import * as React from "react";
import Preloader, { InlineScript } from "../build/preloader/preloader";

export const route = '/';

export const page: StaticPage = (props, clientProps, path, assets, webpackStats) =>

<html lang="en">
<head>
	<meta charSet="utf-8" />
	<title>Shared Room</title>

	<link rel="shortcut icon" type="image/png" href={require("../assets/favicon.png")} />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

	<InlineScript code="props=clientProps" args={{ clientProps }} />
</head>
<body>
	<Preloader path={ assets.script } />

	<header className="header">
		<div><strong>Your Room:</strong> <span id="peerID" className="peer-id">offline</span></div>
		<div>
			<button id="connectButton" className="material-icons button-icon-round" title="Connect to another room">link</button>
			<button id="createButton" className="material-icons button-icon-round" title="Create a room with a copy of this table">content_copy</button>
			<button id="disconnectButton" disabled className="material-icons button-icon-round" title="Go offline">cloud_off</button>
			<button id="peersButton" disabled className="material-icons button-icon-round" title="List people in this room">people</button>
			<button id="saveButton" className="material-icons button-icon-round" title="Save this table to file">file_download</button>
			<button id="loadButton" className="material-icons button-icon-round" title="Load a table from file">file_upload</button>
			<button id="clearButton" className="material-icons button-icon-round" title="Clear this table">delete_sweep</button>
			<button id="imagesButton" className="material-icons button-icon-round" title="Manage pictures">photo_library</button>
			<button id="aboutButton" className="material-icons button-icon-round" title="About this application">info</button>
		</div>
	</header>

	<main className="main">
		<div id="tableSpace" className="table-space" />
	</main>

	<footer className="footer">
		<div id="connectionStatus">Not connected</div>
	</footer>

	<div style={{ display: 'none' }}>
		<div id="aboutDialog" style={{ textAlign: 'center' }}>
			<h1 style={{ fontSize: '2em', lineHeight: 1, margin: '1em 0 0' }}>Shared Board</h1>
			<div style={{ margin: '.5em 0 2em' }}>{ props.version }</div>
			<p>
				Copyright © 2017 Fabio Iotti<br />
				Released under <a href="https://opensource.org/licenses/MIT" target="_blank">The MIT License</a>.
			</p>
			<p>
				Source code available in the <a href="https://gitlab.com/bruce965/shared-board" target="_blank">public repository</a>.
			</p>
			<p>
				This software exists thanks to <a href="http://peerjs.com/" target="_blank">the PeerJS library</a>.
			</p>
			<p>
				If you like this software, please share it.<br />
				If you really like it, consider buying me a pizza.<br />
				<br />
				<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=AHAUDRV2UQRLQ" target="_blank" className="button-primary">Donate</a>
			</p>
		</div>
		<div id="peersDialog">
			<ul id="peersDialogList"></ul>
		</div>
		<div id="loadDialog" style={{ textAlign: 'center' }}>
			<p>Current table will be cleared.</p>
			<p>Do you really want to continue?</p>
			<br />
			<button id="loadDialogCancelButton" className="button-secondary">Cancel</button>
			<button id="loadDialogConfirmButton" className="button-primary">Confirm</button>
		</div>
		<div id="clearDialog" style={{ textAlign: 'center' }}>
			<p>Table will be cleared for every member of this room.</p>
			<p>Do you really want to continue?</p>
			<br />
			<button id="clearDialogCancelButton" className="button-secondary">Cancel</button>
			<button id="clearDialogConfirmButton" className="button-primary">Confirm</button>
		</div>
		<div id="connnectIDDialog" style={{ textAlign: 'center' }}>
			<p>Which room do you want to join?</p>
			<div><input id="connnectIDDialogIDInput" type="text" /></div>
			<br />
			<button id="connnectIDDialogCancelButton" className="button-secondary">Cancel</button>
			<button id="connnectIDDialogConfirmButton" className="button-primary">Confirm</button>
		</div>
		<div id="connectUnsavedDialog" style={{ textAlign: 'center' }}>
			<p>Changing room will clear your current table.</p>
			<p>Do you really want to continue?</p>
			<br />
			<button id="connectUnsavedDialogCancelButton" className="button-secondary">Cancel</button>
			<button id="connectUnsavedDialogConfirmButton" className="button-primary">Confirm</button>
		</div>
		<div id="imagesManageDialog">TODO</div>
	</div>

	<link rel="stylesheet" href={ require("file-loader?name=[name].[hash].css!../style.less") } />

	<script src={ require("file-loader?name=[name].[hash].js!es6-promise/dist/es6-promise.auto.min.js") } />
	<script src={ require("file-loader?name=[name].[hash].js!peerjs/dist/peer.min.js") } />
	<script src={ require("file-loader?name=[name].[hash].js!webfontloader/webfontloader.js") } />
	<script src={ require("file-loader?name=[name].[hash].js!d3/build/d3.min.js") } />
</body>
</html>
