/// <reference path="../../types.d.ts" />

// we use this value to determine when the element can be removed from DOM
// any number higher than the animation's duration is acceptable
const REMOVE_DELAY = 1000;  // ms

/** A context-menu-like box with a non-nestable set of actions. */
export default class ActionsBox {

	/** Element of the currently open menu, if any. */
	private el?: HTMLElement;
	/** Callback function to be called when closing currently open menu, if any. */
	private onClose?: () => void;

	/** Open a box with the defined actions and position, closing the previous one if any. */
	public open(x: number, y: number, actions: { name: string, action: () => void }[]): void {
		this.close();

		this.el = document.createElement('aside');
		this.el.classList.add('actions-box');
		this.el.classList.add('actions-box-faded');
		this.el.setAttribute('role', 'dialog');

		for (const action of actions) {
			const button = document.createElement('button');
			button.classList.add('actions-box-action');
			button.innerText = action.name;
			button.addEventListener('click', action.action);

			this.el.appendChild(button);
		}

		const dismiss = (e: MouseEvent) => {
			this.close();
		};

		const handleKeyUp = (e: KeyboardEvent) => {
			if (e.keyCode == 27) {  // ESC
				e.preventDefault();
				this.close();
			}
		};

		window.addEventListener('click', dismiss);
		window.addEventListener('resize', dismiss);
		window.addEventListener('scroll', dismiss);
		window.addEventListener('keyup', handleKeyUp);

		this.onClose = () => {
			this.onClose = undefined;

			window.removeEventListener('click', dismiss);
			window.removeEventListener('resize', dismiss);
			window.removeEventListener('scroll', dismiss);
			window.removeEventListener('keyup', handleKeyUp);
		};

		document.body.appendChild(this.el);

		// place in correct position
		const elBounds = this.el.getBoundingClientRect();
		const elWidth = elBounds.right - elBounds.left;
		const elHeight = elBounds.bottom - elBounds.top;
		this.el.style.top = `${Math.max(0, Math.min(window.innerHeight - elHeight, y))}px`;
		this.el.style.left = `${Math.max(0, Math.min(window.innerWidth - elWidth, x))}px`;

		this.el.classList.remove('actions-box-faded');
	}

	/** Close current box, if any. */
	public close(): void {
		if (!this.el)
			return;

		const el = this.el;
		this.el = undefined;

		if (this.onClose)
			this.onClose();

		el.classList.add('actions-box-faded');
		// TODO: disable focus
		setTimeout(() => el.remove(), REMOVE_DELAY);
	}
}
