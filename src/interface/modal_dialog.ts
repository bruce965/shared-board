/// <reference path="../../types.d.ts" />

// we use this value to determine when the dialog element can be removed from DOM
// any number higher than the animation's duration is acceptable
const REMOVE_DELAY = 1000;  // ms

/**
 * Manages a modal dialog, trapping focus.
 * 
 * Does not support multiple dialogs open at once.
 */
export default class ModalDialog {

	/** Container of the currently open dialog, if any. */
	private el?: HTMLElement;
	/** Is currently open dialog, if any, dismissable? */
	private dismissable?: boolean;
	/** Callback function to hide the currently open dialog, if any. */
	private dismiss?: () => void;
	/** Element that had focus when current dialog, if any, was open. */
	private focused?: Element;
	/** Callback function to be called when closing currently open dialog, if any. */
	private onClose?: () => void;

	/**
	 * Open a new modal dialog replacing current one if possible.
	 * 
	 * Fails if a non-dismissable dialog is currently open.
	 * 
	 * @returns Promise resolved when the dialog is closed.
	 */
	public open(
		/** Title of the dialog. */
		title: string,
		/** Content of the dialog, will not be cloned. */
		content: HTMLElement,
		/** Allow the user to close the dialog. */
		dismissable = true
	): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			if (this.el) {
				if (!this.dismissable)
					return reject(new Error("Cannot open modal dialog, another non-dismissable already open"));
				
				this.close();
			}

			const dialog = this.createDialog(title, content, dismissable, () => this.close());
			
			this.el = dialog.el;
			this.dismissable = dismissable;
			this.dismiss = dialog.dismiss;
			this.focused = document.activeElement;
			this.onClose = () => resolve();

			document.body.classList.add('modal-dialog-body');
			document.body.appendChild(dialog.el);
			dialog.openModal();  // only open after appending

			this.focusFirst(dialog.el, true);

			requestAnimationFrame(() => {
				dialog.el.classList.remove('modal-dialog-faded');
			});
		});
	}

	/** Close currently open dialog, never fails. */
	public close(): void {
		if (!this.el)
			return;

		const el = this.el;
		this.el = undefined;

		if (this.focused instanceof HTMLElement)
			this.focused.focus();

		el.classList.add('modal-dialog-faded');
		document.body.classList.remove('modal-dialog-body');
		setTimeout(() => el.remove(), REMOVE_DELAY);

		if (this.onClose)
			this.onClose();
	}

	private createDialog(title: string, el: HTMLElement, dismissable: boolean, onDismiss?: () => void) {
		const background = document.createElement('div');
		background.classList.add('modal-dialog-background');

		const closeDialogButton = document.createElement('button');
		closeDialogButton.classList.add('modal-dialog-close');
		closeDialogButton.classList.add('material-icons');
		closeDialogButton.title = "Close dialog";
		closeDialogButton.innerText = 'close';

		const header = document.createElement('header');
		header.classList.add('modal-dialog-header');
		header.innerText = title;

		const content = document.createElement('div');
		content.classList.add('modal-dialog-content');
		content.appendChild(el);

		const dialog = document.createElement('div');
		dialog.classList.add('modal-dialog');
		dismissable && dialog.appendChild(closeDialogButton);
		dialog.appendChild(header);
		dialog.appendChild(content);

		const containerInner = document.createElement('div');
		containerInner.classList.add('modal-dialog-container-inner');
		containerInner.appendChild(dialog);

		const container = document.createElement('div');
		container.classList.add('modal-dialog-container');
		container.appendChild(containerInner);

		const containerOuter = document.createElement('div');
		containerOuter.classList.add('modal-dialog-container-outer');
		containerOuter.appendChild(container);
		containerOuter.addEventListener('click', e => {
			if (e.target == closeDialogButton || (e.target != dialog && !dialog.contains(e.target as Node)))
				dismissable && _onDismiss();
		});

		const root = document.createElement('dialog');
		root.setAttribute('role', 'dialog');
		root.classList.add('modal-dialog-root');
		root.classList.add('modal-dialog-faded');
		root.appendChild(background);
		root.appendChild(containerOuter);

		let alreadyDismissed: boolean;

		const _onDismiss = () => {
			if (alreadyDismissed)
				return;

			alreadyDismissed = true;

			closeModal();

			// TODO: kick out if user focuses inside the dialog while the fade-out animation is running

			if (onDismiss && document.contains(dialog))
				onDismiss();
		};

		let closeModal: () => void;

		const openModal = () => {
			// if browser natively supports modal dialogs we only have to add the fade-out animation,
			// else we also have to emulate the focus trap and the dismiss system

			// FIXME: `root as any` because TypeScript does not define the non-standard `HTMLDialogElement` interface yet.

			if ((root as any).showModal) {
				// browser natively supports modal dialogs

				root.addEventListener('cancel', e => {
					e.preventDefault();

					if (dismissable)
						_onDismiss();
				});

				closeModal = () => {
					(root as any).close();  // close because it's modal and it would block interaction while we run the fade-out animation...
					(root as any).show();  // ...open again but not as modal anymore, we use `pointer-events: none` to disable interaction
				};
				
				(root as any).showModal();
			} else {
				// modal dialogs not supported natively

				// dismiss when pressing ESC button
				const keyUpListener = (e: KeyboardEvent) => {
					if (dismissable && e.keyCode == 27) {
						e.preventDefault();
						_onDismiss();
					}
				};

				// trap focus inside the dialog
				const focusInListener = (e: FocusEvent) => {
					if (root.contains(e.target as Node))
						return;  // still inside

					if (dismissable)
						return _onDismiss();  // close if focusing out of the form

					this.focusFirst(root);
				};

				window.addEventListener('keyup', keyUpListener);
				window.addEventListener('focusin', focusInListener);

				closeModal = () => {
					window.removeEventListener('keyup', keyUpListener);
					window.removeEventListener('focusin', focusInListener);
				};
			}
		};

		return {
			el: root,
			openModal,
			dismiss: _onDismiss
		};
	}

	/** Focus the first focusable child of `container`, if any. */
	private focusFirst(
		container: HTMLElement,
		/** Move the focus "cursor" but don't really focus. */
		virtualFocus = false
	): void {
		// TODO: the "focusable" query is not perfect and poorly tested

		const firstFocusable = container.querySelector('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), object, embed, [tabindex], [contenteditable]') as HTMLElement | undefined;
		if (firstFocusable) {
			firstFocusable.focus();
		
			if (virtualFocus)
				firstFocusable.blur();  // do not show the `:focus` style and don't accept clicks with keyboard
		}

		//return !!firstFocusable;  // return false if nothing to focus
	}
}
