/// <reference path="../../types.d.ts" />

/** Start download of the specified `blob`. */
export const downloadBlob = (
	blob: Blob,
	filename?: string
) => {
	const url = URL.createObjectURL(blob);
	const a = document.createElement('a');
	a.style.display = 'none';
	a.href = url;
	if (filename)
		a.download = filename;
	document.body.appendChild(a);
	a.click();
	a.remove();
	URL.revokeObjectURL(url);
}

let blobUploadInput: HTMLInputElement|undefined;
let blobUploadCallback: ((e: Event) => void) | undefined;

/** Open a file selection dialog. */
export const pickFile = (
	/** Callback function. Not called when dialog is closed without selecting a file. */
	cb: (file: File) => void
) => {
	if (!blobUploadInput) {
		const input = document.createElement('input');
		input.type = 'file';
		input.style.opacity = '0';  // make invisible
		input.style.position = 'fixed';
		input.style.top = '-10px';
		input.style.left = '-10px';
		input.tabIndex = -1;  // make not tabbable
		input.style.pointerEvents = 'none';  // make not clickable
		document.body.appendChild(input);

		blobUploadInput = input;
	} else {
		blobUploadInput.value = '';
	}

	if (blobUploadCallback)
		blobUploadInput.removeEventListener('change', blobUploadCallback);
	
	let alreadySelected: boolean;
	blobUploadCallback = e => {
		if (alreadySelected)
			return;
		
		alreadySelected = true;

		const files = (blobUploadInput as HTMLInputElement).files && (blobUploadInput as HTMLInputElement).files;
		if (files && files.length)
			cb(files.item(0));
	};

	blobUploadInput.addEventListener('change', blobUploadCallback);

	blobUploadInput.click();
}
