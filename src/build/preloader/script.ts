// this script will be inlined as an IIFE, the following is a parameter
declare const path: string;

/** How long to wait before removing preloader element, must be higher that fade-out animation duration. */
const CLEANUP_DELAY = 350;  // ms

const total = 5;  // 4 fonts + 1 script
let loaded = 0;

const resourceLoaded = () => {
	if (++loaded == total) {
		setTimeout(() => {
			document.getElementsByClassName('preloader-loading-splash')[0].remove();
		}, CLEANUP_DELAY);

		document.body.classList.remove('preloader-loading');
	}
};

window.addEventListener('load', () => {
	WebFont.load({
		fontactive: resourceLoaded,
		fontinactive: resourceLoaded,
		timeout: 15000,
		google: {
			families: ['Roboto Mono', 'Roboto', 'Roboto:bold', 'Material Icons']
		}
	});

	const script = document.createElement('script');
	script.onload = resourceLoaded;
	script.src = path;
	document.head.appendChild(script);
});
