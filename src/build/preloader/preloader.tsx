import * as React from "react";
import { Component } from "react";

export class InlineScript extends Component<{ code: string, args?: { [key: string]: any } }> {
	public render() {
		const keys: string[] = [];
		const values: any[] = [];

		if (this.props.args) {
			for (const key in this.props.args) {
				keys.push(key);
				values.push(this.props.args[key]);
			}
		}

		return <script dangerouslySetInnerHTML={{
			__html: '(function(' + keys.join(",") + '){' + this.props.code + '})(' + values.map(value => JSON.stringify(value)).join(",") + ')'
		}} />;
	}
}

export default class PreloaderComponent extends Component<{ path: string }> {

	public render(): JSX.Element {
		return <div className="preloader-loading-splash">
			<InlineScript code={`document.body.classList.add('preloader-loading')`} />
			<style>{ require('raw-loader!./style.less') }</style>

			<svg width="50" height="50" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
				<circle cx="25" cy="25" r="22"></circle>
			</svg>

			<InlineScript
				code={ require('raw-loader!./script.ts') }
				args={{
					path: this.props.path
				}}
			/>
		</div>;
	}
}
