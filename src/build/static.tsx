/// <reference path="../../types.d.ts" />

import * as React from "react";
import { renderToStaticMarkup } from "react-dom/server";
import * as index from "../page/index";

const routes: { [route: string]: StaticPage } = {
	[index.route]: index.page
};

export = (locals: any) => {
	let page: StaticPage | undefined;
	for (const route in routes) {
		if (locals.path == route) {
			page = routes[route];
			break;
		}
	}

	if (!page)
		throw new Error(`missing route: '${locals.path}'`);

	return '<!DOCTYPE html>' + renderToStaticMarkup(
		page(
			locals.serverProps,
			locals.clientProps,
			locals.path,
			locals.assets,
			locals.webpackStats
		)
	);
};
