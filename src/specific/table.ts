/// <reference path="../../types.d.ts" />

//import * as d3 from "d3-zoom";

const TABLE_WIDTH = 600;  // px
const TABLE_HEIGHT = 420;  // px
const TABLE_PADDING = 10;  // px

const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';

export default class Table {

	private readonly handleResize: () => void;

	constructor(container: HTMLElement) {
		// Structure:
		// 	svg viewBox=... (viewBox = size of the table + padding)
		// 		g transform="translate scale"
		// 			rect x=0 y=0 width=... height=... (size of the table)
		// 			... (everything added to the table is here)

		const svg = document.createElementNS(SVG_NAMESPACE, 'svg');
		//svg.setAttribute('viewBox', '0 0 0 0');  // patch Firefox 53.0
		//svg.viewBox.baseVal.x = -TABLE_PADDING;
		//svg.viewBox.baseVal.y = -TABLE_PADDING;
		//svg.viewBox.baseVal.width = TABLE_WIDTH + TABLE_PADDING*2;
		//svg.viewBox.baseVal.height = TABLE_HEIGHT + TABLE_PADDING*2;
		svg.style.width = '100%';
		svg.style.height = '0';  // patch Firefox 53.0
		svg.style.minHeight = '100%';
		svg.style.backgroundColor = 'gray';

		const g = document.createElementNS(SVG_NAMESPACE, 'g');
		svg.appendChild(g);

		const table = document.createElementNS(SVG_NAMESPACE, 'rect');
		table.x.baseVal.value = 0;
		table.y.baseVal.value = 0;
		table.width.baseVal.value = TABLE_WIDTH;
		table.height.baseVal.value = TABLE_HEIGHT;
		table.style.fill = 'white';
		g.appendChild(table);

		var a = document.createElement('div');
		a.innerHTML = `<rect x="10" y="10" width="53" height="85" rx="5" fill="lime" stroke="black" stroke-width="1" />`;
		table.appendChild(a.firstChild as SVGElement);

		container.appendChild(svg);

		const d3svg = d3.select(svg);
		const d3zoom = d3.zoom()
			.scaleExtent([1/4, 8])
			.translateExtent([[-TABLE_PADDING, -TABLE_PADDING], [TABLE_WIDTH + TABLE_PADDING, TABLE_HEIGHT + TABLE_PADDING]])
			.on('zoom', () => g.setAttribute('transform', d3.event.transform));

		d3svg.call(d3zoom);
		this.handleResize = () => d3zoom.scaleBy(d3svg, 1);

		// HACK: for some reason calling `recompute` now would not work even though `container` is already in the DOM, we have to retry after a delay.
		setTimeout(() => this.recompute(), 20);
	}

	/* Call this function after appending the container to the DOM and whenever the container is resized. */
	public recompute(): void {
		this.handleResize();
	}

	isEmpty(): boolean {
		return false;
	}

	clear(): void {

	}

	getBlob(): Blob {
		const blob = new Blob(["TODO"]);
		return blob;
	}

	loadBlob(blob: Blob) {

	}
}
