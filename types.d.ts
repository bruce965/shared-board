
// HACK: there probably is a better way to declare import of a module to global scope.
const __d3 = import('d3');
declare const d3 = await __d3;

interface StaticServerProperties {
	version: string;
}

interface StaticClientProperties {
	peerJSAPIKey: string;
}

declare const props: StaticClientProperties;

interface StaticPage {
	(
		props: StaticServerProperties,
		clientProps: StaticClientProperties,
		path: string,
		assets: { [name: string]: string },
		webpackStats: any
	): JSX.Element;
}
